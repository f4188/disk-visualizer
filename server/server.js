
const express = require("express")
const path = require("path")

Files = require('./files.js').Files

function DiskAppServer( port ) {

	this.files = null

	this.port = port
	this.app = express()
	this.router = express.Router()

	this.router.get("/", (req, res) => { res.sendFile( path.join( __dirname + "/../client/index.html") ) } )
	this.router.get("/app.js", (req, res) => { res.sendFile( path.join( __dirname + "/../client/app.js") ) } )
	this.router.get("/bootstrap.css", (req, res) => { res.sendFile( path.join( __dirname + "/../node_modules/bootstrap/dist/css/bootstrap.min.css" ))})
	this.router.get("/:path/:r", (req, res) => { 

		let file = this.files.get( req.params.path, req.params.r )
		res.json( file )

	})

	this.app.use("/files", this.router)

}

DiskAppServer.prototype.init = function( root, port ) {

	this.files = new Files( root )
	this.files.build()

	port = port || this.port
	this.app.listen(port)

}

module.exports = {

	DiskServer : DiskAppServer

}
