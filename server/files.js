
const async = require('async')
const fs = require('fs')
const promisify = require("es6-promisify")
const path = require("path")

const getFileStat = promisify( fs.stat )
const readDirectory = promisify( fs.readdir )

function Files( root ) {

	this.root = root
	this.fileMap = new Map()
	this.queue = null

}

Files.prototype.fillSize = function ( root ) { 

	let file = this.fileMap.get( root || this.root )

	if( !file || file.isDirectory && file.children == null && !file.err) // not in map or children not read
		return -1

	if( file.err && file.err.code == "ENOENT" || file.err ) //any error EPERM ENOENT
		file.size = 0

	else if( file.isDirectory && file.size == null ) {

		let childSizes = file.children.map( childPath => { 

			let childSize = this.fillSize( childPath )
			return (childSize == undefined || Number.isNaN(childSize)) ? 0 : childSize // -1 , 0 or > 0
			
		})

		if( childSizes.some( x => x == -1 ) )
			return -1
		
		file.size = childSizes.reduce( (sum, x) => sum + x, 0 )

	} 
		
	return file.size

}

Files.prototype.traverse = async function ( task, callback ) {

	let file = { path : task.file }

	try {

		let stat = await getFileStat( task.file )
		file = { path : task.file , isDirectory : stat.isDirectory(), size : stat.size, atime: stat.atime, mtime : stat.mtime, ctime : stat.ctime }

		if ( !file.isDirectory )
			file.type = path.extname( file.path )

		else {

			file.size = null
			file.children = null

			let childFileNames = await readDirectory( file.path )
			let childFilePaths = childFileNames.map( childPath => path.join( file.path, childPath ) )
			file.children = childFilePaths
			childFilePaths.forEach( file => this.queue.push( { file : file } ) )

		}

	} catch ( error ) {

		file.err = error.code

	}

	this.fileMap.set( file.path, file )
	callback()

}

Files.prototype.get = function ( path, recur ) {

	this.fillSize( path )

	let file = this.fileMap.get( path )

	if( !file )
		return {}

	if( !file.isDirectory || !file.children || file.children.length == 0 ) //not directory or empty directory
		return file

	let ret =  Object.assign( {}, file ), files = [ ret ]

	while( recur -- > 0 ) {

		new_files = []

		for ( let file of files ) {

			let childFiles = file.children.map( child => {

				let childFile = this.fileMap.get( child )
				if( childFile )
					return Object.assign({}, childFile ) 

			}).filter( x => x )

			file.children = ( recur != 0 ) ? childFiles : null
			new_files =  new_files.concat( childFiles.filter( childFile => childFile.isDirectory && childFile.children && childFile.children.length > 0 ) )

		}

		files = new_files

		if( new_files.length == 0 ) //recur not zero but no more n-greatchildren
			break

	}

	return ret

}

Files.prototype.build = function () {

	this.queue = async.queue( (this.traverse).bind(this), 100 )
	this.queue.push( { file : this.root } )

}

module.exports = {

	'Files' : Files

}