
const path = require("path")

module.exports = {

	entry: './client/app.js',
	output : {
		filename: 'app.js',
		path: path.resolve( __dirname, "client")
	}

}