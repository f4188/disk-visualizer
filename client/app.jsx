
import React from "../node_modules/react";
import ReactDOM from "../node_modules/react-dom";
import $ from "../node_modules/jquery"
window.jQuery = window.$ = global.jQuery = $
const bootstrap = require("../node_modules/bootstrap")
import * as d3 from "d3"

//String.prototype.chunk = (size) => [].concat.apply([], this.split('').map( (x,i) => i % size ? [] : this.slice(i, i + size) ), this)

String.prototype.chunk = function(size) {
    return [].concat.apply([],
        this.split('').map(function(x,i){ return i%size ? [] : this.slice(i,i+size) }, this)
    )
}

var numSteps = 3
var defaultURL = "http://localhost:9003/files/C%3A%5CUsers%5CFawaz A%5CDocuments%5CGitHub/"
let tilingOpt = d3.treemapResquarify
let tilingList = [ d3.treemapResquarify, d3.treemapBinary, d3.treemapDice , d3.treemapSlice, d3.treemapSquarify ]

const Visual = async function() {

	let height = window.innerHeight, diskName = "C:\\", appStyle =	window.getComputedStyle(document.getElementById("app"))
	let width = parseFloat( appStyle.width) - 30 - parseFloat(appStyle.paddingLeft)- parseFloat(appStyle.paddingRight)


	var onClickStepInput = () => { 

		numSteps = parseInt(document.getElementById("numSteps").value)
		ReactDOM.render(visual, element, () => step(defaultURL + numSteps))
	}	

	var onClickTiling = () => {

		let index = document.getElementById("tiling").selectedIndex 	
		tilingOpt = tilingList[index]
		ReactDOM.render(visual, element, () => step(defaultURL + numSteps))

	}

	let visual = <div className="container-fluid" >

		<div>

			<label htmlFor="numSteps" style={{"paddingRight" : 10}}> Step levels </label>
			<input id="numSteps" type="number"/>
			<button onClick={onClickStepInput} > Update step levels </button>

			<label htmlFor="tiling" style={{"paddingRight" : 10, "paddingLeft" : 20}}> Tiling </label>
			<select id="tiling">
				<option value="Resquarify">Resquarify</option>
			  <option value="Binary">Binary</option>
			  <option value="Dice">Dice</option>
			  <option value="Slice">Slice</option>
			  <option value="Squarify">Squarify</option>
			</select>
			<button onClick={onClickTiling} > Update tiling </button>

		</div>

	  	<div className="row">
	  		<div className="col">
				<svg height={height} width={width}>
				
				</svg>
			</div>
		</div>

	</div>
	
	const element = document.getElementById("app")

	let transitioning = false

	let step = async ( filePath, data, newPath ) => {

		console.log(filePath)
		let files

	/*	function colObj() {

			this.i = 3
			//this.colorArr = ["#ecffe6", "#d8ffcc", "#c4ffb3", "#b1ff99", "#9dff80", "#8aff66", "#76ff4d", "#63ff33", "#4fff1a", "#3cff00", "#36e600", "#30cc00", "#2ab300", "#249900", "#1e8000", "#186600"].map( x => d3.color(x) )
			this.colorArr = ["#ecffe6", "#c4ffb3",  "#9dff80", "#76ff4d",  "#4fff1a",  "#36e600",  "#2ab300",  "#1e8000", "#186600"].map( x => d3.color(x) )
			this.color = () => this.colorArr[this.i]			
			this.darker = () => { 
				let ret = new colObj() 
				ret.i = this.i + 1  
				return ret 
			}

		}*/
		 
		if(filePath) {
			let response = await fetch( filePath, { method : "get" } )
			files = await response.json()
		} else 
			files = data

		let fileSizeLowerBound = ( files ) => files.size ? files.size : files.children.reduce( (sum, child) => fileSizeLowerBound(child), 0)

		let smallestVisisbleFile = files.size ? files.size / 1e3 : files

		let condenseFiles = ( files ) => {

			if( ! files.children )
				return

			let others = files.children.filter( child => child.size < smallestVisisbleFile), cFiles = files.children.filter( child => child.size >= smallestVisisbleFile)
			if( others.length > 0) {

				let condenseFiles = Object.assign( {}, files )
				condenseFiles.children = others
				condenseFiles.size = others.reduce( (sum, child) => sum + child.size, 0 )

				cFiles.push( { path : files.path + "\\other" , size : others.reduce( (sum, child) => sum + child.size, 0), isDirectory : false, children : null, condenseFiles :  condenseFiles } )
			}

			files.children = cFiles
			cFiles.forEach( file => condenseFiles(file) )

		}

		condenseFiles( files )

		let colorizeFiles = ( files, color ) => {

			files.color = color
			if( files.children )
				files.children.forEach( file => colorizeFiles( file, color.darker()))//color.darker() ))
			
		}

		colorizeFiles(files, d3.color("#b1ff99") )//new colObj())
		
		let svg = d3.select("svg")

		let treemap = d3.treemap().tile( tilingOpt ).size( [width, height] ).round( true ).padding( 2 )
		let root = d3.hierarchy(files, ( data ) => data.isDirectory && data.children && data.children.length > 0 ? data.children : null ).sum( ( d ) => d.children ? 0 : d.size )
		treemap(root)

		console.log(files)

		let parentNodes = ((files.path != "C:\\") ? files.path.split("\\") : [ files.path ] ).map( (file, i) => { 

			return { 

			children : null, 
				data : 
				{ 
				path : (files.path != "C:\\") ? files.path.split("\\").slice(0, i + 1).join("\\") : files.path, 
				color : d3.color("#b1ff99")/*new colObj() */, 
				size : null 
				}, 
			x0 : 0, 
			x1 : width,
			y0 : i * 20, 
			y1 : (i * 20 + 20), 
			parent : null, 
			value : 0  

			}
		})
		
		console.log(parentNodes)
		parentNodes[0].data.path = "C:\\"

		parentNodes.forEach( parentNode => { 

			parentNode.parent = parentNode; 
			parentNode.depth = 1 

		})

		let nodes = parentNodes.concat( root.descendants().map( node => {

			node.y0 = node.y0 + parentNodes.length * 20
			node.y1 = node.y1 + parentNodes.length * 20
			return node

		}))

		svg.selectAll("g").data( nodes, (d) => d.data.path ).transition().duration(750)
		.attr("transform", null).attr( "transform" , (d) => "translate(" + d.x0 + "," + d.y0 + ")" )
		svg.selectAll("rect").data(nodes, (d) => d.data.path).transition().duration(750)
		.attr("width", (d) => d.x1 - d.x0).attr("height", (d) => d.y1 - d.y0).attr("fill", d =>   d.data.color )//.color() )
		svg.selectAll("text").data(nodes, (d) => d.data.path).remove()
		let oldG = svg.selectAll("g").data( nodes, (d) => d.data.path )

		svg.selectAll("g").data( nodes, (d) => d.data.path ).exit().remove() 

		setTimeout( () => {

			oldG.append("text").selectAll("tspan").data( d => makeText(d) ).enter()
			.append("tspan").attr("x", 4 ).attr("y", (d, i) => 13 + i * 11 ).attr("font-size", 10).attr("font-family", "Helvetica, Arial").text( d => d );

		}, 750 )


		let cell = svg.selectAll("g").data( nodes, ( d ) => d.data.path ).enter().append("g").attr("id", d => d.data.path ).attr("transform", (d) => "translate(" + d.x0 + "," + d.y0 + ")" )
		cell.append("rect").attr("width", (d) => d.x1 - d.x0 ).attr("height", (d) => d.y1 - d.y0 ).attr("fill", d => d.data.color )///.color())
		cell.style("fill-opacity", 0)

		cell.on("click", async (d) => {

			if(transitioning) return

			transitioning = true
			let newPath = d.data.path.split("\\").slice(0, files.path.split("\\").length + 1).join("\\")

			await step( "http://localhost:9003/files/" + newPath.split(":").join("%3A").split("\\").join("%5C") + "/" + numSteps , null, newPath )//, flatNodeList)// firstChildNode.children)

	
			setTimeout( ()=> { transitioning = false }, 750)
			//t1.remove()//.each("end", function() {
        		//svg.style("shape-rendering", "crispEdges");
        //		transitioning = false;
      	//	})

		})
	
		cell.append("text").selectAll("tspan").data( d => makeText(d) ).enter().append("tspan").attr("x", 4 )
		.attr("y", (d, i) => 13 + i * 11 ).attr("font-size", 10).attr("font-family", "Helvetica, Arial")
		.text( d => d );

		cell.transition().delay(750).style("fill-opacity", 1)

		return cell

	}

	ReactDOM.render(visual, element, () => step("http://localhost:9003/files/C%3A%5CUsers%5CFawaz A%5CDocuments%5CGitHub/" + numSteps) )

}

Visual()

function makeText ( d ) {

	if( d.children || d.isDirectory )
		return ""

	if ( d.data.size == null )
		return [ "\u219A " +  d.data.path ]

	let size 
	if (d.data.size > 2 ** 20)  
		size = Math.round( (d.data.size / 2 ** 20)  )  + "MB"
	else if ( d.data.size > 2 ** 10 )
		size = Math.round( (d.data.size / 2 ** 10) )  + "KB"
	else 
		size = Math.round( d.data.size) + "B" 

	return (size + " " + d.data.path.split("\\").slice(-1)[0]).chunk(  Math.max((d.x1 - d.x0) - 13, 0) / 5  ).slice(0, (d.y1 - d.y0) /  20 )

}
